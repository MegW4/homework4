﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using HW4.Models;

namespace HW4.ViewModel
{
    public class CellsViewModel
    {
        public List<ImageCellItem> Cells { get; set; }

        public CellsViewModel()
        {
            Cells = new ImageCellItem().GetCellItem();
        }
    }
}
