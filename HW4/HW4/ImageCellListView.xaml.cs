﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using HW4.Models;
using HW4.ViewModel;

namespace HW4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ImageCellListView : ContentPage
	{
        CellsViewModel vm;
        public ImageCellListView()
        {
            InitializeComponent();
            vm = new CellsViewModel();
            ImageCellsListView.ItemsSource = vm.Cells;
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            ImageCellItem itemTapped = (ImageCellItem)listView.SelectedItem;
            var uri = new Uri(itemTapped.url);
            Device.OpenUri(uri);
        }
    }
}