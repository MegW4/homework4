﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace HW4.Models
{
    public class ImageCellItem
    {
        public string ImageText{get; set;}
        public string IconSource {get; set;}
        public string ShortDescription{get; set;}
        public string url { get; set;}

        public List<ImageCellItem> GetCellItem()
        {
            List<ImageCellItem> cells = new List<ImageCellItem>()
            {
                new ImageCellItem()
                {
                    ImageText = "Anjanath", ShortDescription = "The Anjanath patrols the Ancient Forest looking for its favorite meal, Aptonoth. This belligerent monster will attack anything without the slightest provocation.",
                    IconSource = "https://monsterhunterworld.wiki.fextralife.com/file/Monster-Hunter-World/anjanath-monster-hunter-world-large-monster.jpg",
                    url = "https://monsterhunterworld.wiki.fextralife.com/Anjanath"
                },
                new ImageCellItem()
                {
                    ImageText = "Diablos", ShortDescription = "The apex monster of the Wildspire Waste.A menacing, territorial beast that lurks underground. Loud noises will cause it to lunge out of the sand in search of prey.",
                    IconSource = "https://monsterhunterworld.wiki.fextralife.com/file/Monster-Hunter-World/diablos_monster_hunter_world_large.jpg",
                    url = "https://monsterhunterworld.wiki.fextralife.com/Bazelgeuse"
                },
                new ImageCellItem()
                {
                    ImageText = "Bazelgeuse", ShortDescription = "A nefarious flying wyvern that travels the New World in search of prey. It scatters explosive scales over a wide area to prey one whatever gets caught in the blast.",
                    IconSource = "https://monsterhunterworld.wiki.fextralife.com/file/Monster-Hunter-World/Bazelgeuse-large-monster-hunter-world-mhw.jpg",
                    url = "https://monsterhunterworld.wiki.fextralife.com/Diablos"
                },
                new ImageCellItem()
                {
                    ImageText = "Kirin", ShortDescription = "Kirin are so rarely sighted that little is known of their ecology. It's been said they envelop themselves in pure electricity when they are provoked.",
                    IconSource = "https://monsterhunterworld.wiki.fextralife.com/file/Monster-Hunter-World/kirin-monster-hunter-world-large-monster.png",
                    url = "https://monsterhunterworld.wiki.fextralife.com/Kirin"
                },
                new ImageCellItem()
                {
                    ImageText = "Kushala Daora", ShortDescription = "An elder dragon that shields itself with fierce winds, preventing anyone from approaching it. Its skin consists of hard, metallic scales.",
                    IconSource = "https://monsterhunterworld.wiki.fextralife.com/file/Monster-Hunter-World/kushala-daora-large-monster-hunter-world-mhw.jpg",
                    url = "https://monsterhunterworld.wiki.fextralife.com/Kushala+Daora"
                },
                new ImageCellItem()
                {
                    ImageText = "Nergigante", ShortDescription = "A terrible elder dragon that appears when other elders are in the vicinity. Its penchant for destruction is well documented.",
                    IconSource = "https://monsterhunterworld.wiki.fextralife.com/file/Monster-Hunter-World/nergigante-large-monster-hunter-world-mhw.jpg",
                    url = "https://monsterhunterworld.wiki.fextralife.com/Nergigante"
                },
                new ImageCellItem()
                {
                    ImageText = "Teostra", ShortDescription = "A brutal elder dragon wreathed in flames that spits blazing fire. Teostra are of such a fierce and deadly nature that the Guild closely monitors their movements.",
                    IconSource = "https://monsterhunterworld.wiki.fextralife.com/file/Monster-Hunter-World/Teostra-large-monster-hunter-world-mhw.jpg",
                    url = "https://monsterhunterworld.wiki.fextralife.com/Teostra"
                },
                new ImageCellItem()
                {
                    ImageText = "Xeno'Jiiva", ShortDescription = "Xeno'jiiva is an Elder Dragon and the Final Boss in the Story Mode of Monster Hunter World .",
                    IconSource = "https://monsterhunterworld.wiki.fextralife.com/file/Monster-Hunter-World/xenojiiva-monster-hunter-world-large-monster.jpg",
                    url = "https://monsterhunterworld.wiki.fextralife.com/Xeno'jiiva"
                },
            };
            return cells;
        }

    }
}
